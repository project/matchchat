
Module to plug in the comments system of the Matchchat.co.uk API

Built-in support for:
 - Matchchat comments API

Installation
------------

Copy matchchat module to your module directory and then enable on the admin
modules page.  "/admin/modules"

Visit the block admin page, and add the block "Matchchat comments area" to the
region of your choice. "/admin/structure/block"

Author
------
Chris Scudder
chris@f34r.co.uk
